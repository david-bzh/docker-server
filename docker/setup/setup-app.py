#!/usr/bin/python3
import os
os.system("python3 -m venv env && . env/bin/activate");
os.system("pip -q install -Uq inquirer");
import inquirer

# default value 
app_name = 'app1'
app_public = ''
dir_project = '../'
server_name = 'apache'
list_app = ['Symfony', 'Laravel', 'Bedrock', 'Wordpress' ];

# create file .env
def create_file_env():
    f = open(".env","w+")
    f.write('APP_NAME='+app_name+'\n')
    f.write('APP_PUBLIC='+app_public+'\n')
    f.write('PROJECT_DIR='+dir_project+'\n')
    f.write('SERVER_NAME='+server_name+'\n')

# create app
r = inquirer.prompt( [
        inquirer.List('choices_app',
            message="Select a framework and Cms:",
            choices=list_app,
        ),
    ])

if r['choices_app'] == "Symfony":
    r = inquirer.prompt( [
        inquirer.List('symfony_v',
            message="Select a framework and Cms:",
            choices=['website-skeleton','skeleton'],
        ),
    ])
    os.system('git clone https://github.com/symfony/' +  r['symfony_v'] +' app');
    app_public = 'public'

elif r['choices_app'] == "Laravel":
    os.system('git clone https://github.com/laravel/laravel.git app');
    app_public = 'public'

elif r['choices_app'] == "Bedrock":
    os.system('git clone https://github.com/roots/bedrock.git app');
    app_public = 'web'

elif r['choices_app'] == "Wordpress":
    os.system('git clone https://github.com/WordPress/WordPress.git app');

create_file_env();
