.PHONY = help
.DEFAULT_GOAL = help

##Docker:
docker-up: ## Create and start containers
	docker-compose --project-directory ./docker up -d

docker-stop: ## Stop services
	docker-compose --project-directory ./docker stop

docker-start: ## Start services
	docker-compose --project-directory ./docker start

docker-down: ## Stop and remove containers
	docker-compose --project-directory ./docker down

docker-down-v: ## Stop and remove containers and volumes
	docker-compose --project-directory ./docker down -v

docker-v-ls: ## List volumes
	docker volumes ls

docker-v-rm: ## Remove volumes by --name
	docker volumes rm

docker-ps: ## List containers
	docker ps

docker-update: ## Update config
	@echo not create

##App:
install:
	docker run -it --rm --name script-install-server -v "$(PWD)/docker/setup":/usr/src/myapp -w /usr/src/myapp python:3 python setup-app.py

help: ## Print help on Makefile
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-20s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'



